import {
  Component,
  OnInit,
  HostListener,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Template } from '@interface/template';
import { TemplateService } from '@module/template-generator/services/template.service';

@Component({
  selector: 'app-template-viewer',
  templateUrl: './template-viewer.component.html',
  styleUrls: ['./template-viewer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TemplateViewerComponent implements OnInit {
  @ViewChild('templateRef', { static: true }) templateRef: ElementRef;

  public template: Template = null;
  public selectedElement: any = null;
  public selectedElementTemplate: any = null;
  @HostListener('click', ['$event'])
  onComponentClick($event): void {
    const target = $event.target as HTMLElement;
    if (target.className.includes('editable')) {
      this.selectedElement = target;
      this.selectedElementTemplate = target.outerHTML;
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private ts: TemplateService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { template: Template }) => {
      this.template = data.template;
    });
  }

  updateSelectedTemplate(template: string): void {
    this.selectedElement.innerHTML = template;
    this.selectedElementTemplate = '';
    this.ts.updateTemplate({
      id: this.template.id,
      modified: new Date().getTime(),
      template: this.templateRef.nativeElement.outerHTML,
    });
  }
}
