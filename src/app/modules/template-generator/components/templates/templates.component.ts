import { Component, OnInit } from '@angular/core';
import { TemplateService } from '@module/template-generator/services/template.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Template } from '@interface/template';
import * as moment from 'moment';
import { TIME_FORMAT } from '@const/tempate-modified-date';
import { TemplateGeneratorsRoutes } from '@module/template-generator/template-generator-routes';
@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss'],
})
export class TemplatesComponent implements OnInit {
  public templates: Template[] = [];
  displayedColumns: string[] = ['id', 'name', 'modified'];
  constructor(
    private ts: TemplateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private tgRoutes: TemplateGeneratorsRoutes
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { templates: Template[] }) => {
      this.templates = data.templates;
    });
  }
  formatDate(timestamp: number): string {
    return moment(timestamp).format(TIME_FORMAT);
  }
  selectTemplate(template: Template): void {
    this.router.navigate([`${this.tgRoutes.VIEWER}`, template.id]);
  }
}
