import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  ViewEncapsulation,
  Renderer2,
} from '@angular/core';
import { cloneDeep } from 'lodash';
@Component({
  selector: 'app-template-editor',
  templateUrl: './template-editor.component.html',
  styleUrls: ['./template-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TemplateEditorComponent implements OnInit {
  @ViewChild('editable', { static: true }) editable: ElementRef;
  @Output() updateTemplate: EventEmitter<string> = new EventEmitter<string>();
  @Input() template;
  @Output() templateSave: EventEmitter<HTMLElement> = new EventEmitter<
    HTMLElement
  >();
  public fontSize = 10;
  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {
    this.fontSize =
      Number(
        window
          .getComputedStyle(this.editable.nativeElement)
          .fontSize.replace('px', '')
      ) || 10;
  }
  fontChange(): void {
    this.editable.nativeElement.style.fontSize = `${this.fontSize}px`;
  }
  saveTemplate(): void {
    this.renderer.addClass(this.editable.nativeElement, 'editable');
    const resultTemplate = cloneDeep(this.editable.nativeElement.outerHTML);
    this.updateTemplate.emit(resultTemplate);
  }
}
