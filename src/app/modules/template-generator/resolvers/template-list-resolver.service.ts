import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Template } from '@interface/template';
import { TemplateService } from '../services/template.service';

@Injectable({
  providedIn: 'root',
})
export class TemplateListResolverService
  implements Resolve<Observable<Template[]>> {
  constructor(private ts: TemplateService) {}
  resolve(): Observable<Template[]> {
    return this.ts.getTemplates();
  }
}
