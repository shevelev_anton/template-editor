import { TestBed } from '@angular/core/testing';

import { TemplateListResolverService } from './template-list-resolver.service';

describe('TemplateListResolverService', () => {
  let service: TemplateListResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TemplateListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
