import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VIEWER } from './template-generator-routes';
import { TemplatesComponent } from './components/templates/templates.component';
import { TemplateListResolverService } from './resolvers/template-list-resolver.service';
import { TemplateViewerComponent } from './components/template-viewer/template-viewer.component';
import { TemplateResolverService } from './resolvers/template-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: TemplatesComponent,
    resolve: {
      templates: TemplateListResolverService,
    },
  },
  {
    path: `${VIEWER}/:id`,
    component: TemplateViewerComponent,
    resolve: {
      template: TemplateResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemplateGeneratorRoutingModule {}
